# @slugbugblue/point

## 1.0 - 2023-03-15

- Pulled out of `@slugbugblue/trax` into its own repository for an initial
  independent release
