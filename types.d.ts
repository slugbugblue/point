/** Type information.
 * @copyright 2022-2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

/** A point-like object has numerical x and y properties. */
type PointLike = {
  x: number
  y: number
}
